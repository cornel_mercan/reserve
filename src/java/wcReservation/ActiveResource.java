/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.security.Principal;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * REST Web Service
 *
 * @author Cornel
 */
@Path("active")
public class ActiveResource {

    @Context
    SecurityContext securityContext;

    /**
     * Creates a new instance of GenericResource
     */
    public ActiveResource() {

    }

    @GET
    @Secured
    @Produces(MediaType.TEXT_PLAIN)
    public String getClasses() {

        if (Database.sessions.isEmpty()) {
            return "No timer scheduled";
        }
        
        Principal principal = securityContext.getUserPrincipal();
        String username = principal.getName();
        NewWorldClassReservation wcr = Database.getUserData(username).wcr;
        if(wcr == null) {
            return "Something went wrong";
        }
        
        if (wcr.reserveScheduled) {
            return "Timer scheduled: " + wcr.scheduledClass.title + ", " + wcr.scheduledClass.classDay.name +
                    " on " + wcr.scheduledClass.hours + " at " + wcr.scheduledClass.clubId;
        }
        else {
            return "No timer scheduled";
        }
    }
    
    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putClass(String content) {
    }
}
