/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Cornel
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(wcReservation.ActiveResource.class);
        resources.add(wcReservation.AuthenticationEndpoint.class);
        resources.add(wcReservation.AuthenticationFilter.class);
        resources.add(wcReservation.CancelResource.class);
        resources.add(wcReservation.ClassResource.class);
        resources.add(wcReservation.ClubResource.class);
        resources.add(wcReservation.DatabaseResource.class);
        resources.add(wcReservation.MakeReservationResource.class);
    }
    
}
