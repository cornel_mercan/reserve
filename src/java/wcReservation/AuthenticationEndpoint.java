/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Cornel
 */
@Path("login")
public class AuthenticationEndpoint {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public AuthenticationEndpoint() {
        //Database.initDatabase();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String login() {

        return "dummy";
    }
    

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public Response authenticateUser(@FormParam("username") String usernameStr, 
                                     @FormParam("password") String password) {

        try {
            
            String username = usernameStr.toLowerCase();

            // Authenticate the user using the credentials provided
            authenticate(username, password);

            // Issue a token for the user
            String token = issueToken();
            NewWorldClassReservation wcr;
            User loginUser = new User(username, password);
            UserData ud = Database.getUserData(username);
            if (ud != null && ud.user.password.equals(password)) {
                //user exists; just update token
                Database.removeUser(ud.token);
                Database.removeUserData(username);
                wcr = ud.wcr;
                
                ud.wcr.login();
            }
            else if (ud != null) {
                //different password
                wcr = new NewWorldClassReservation(loginUser);
                wcr.login();
                
                Database.removeUser(ud.token);
                Database.removeUserData(username);
            }
            else {
                // user not in database
                wcr = new NewWorldClassReservation(loginUser);
                wcr.login();
            }
            
            Database.addUser(token, loginUser);
            Database.addUserData(username, new UserData(loginUser, wcr, token));

            // Return the token on the response
            return Response.ok(token).build();

        } catch (Exception e) {
            Logger.getLogger(AuthenticationEndpoint.class.getName()).log(Level.SEVERE, null, e);
            return Response.status(Response.Status.FORBIDDEN).build();
        }      
    }
    
    private void authenticate(String username, String password) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
        Logger.getLogger(AuthenticationEndpoint.class.getName()).log(Level.INFO, 
                "Authenticating user " + username + "/" + password);
    }

    private String issueToken() {
        // Issue a token (can be a random String persisted to a database or a JWT token)
        // The issued token must be associated to a user
        // Return the issued token
        Random random = new SecureRandom();
        String token = new BigInteger(130, random).toString(32);
        
        return token;
    }
}
