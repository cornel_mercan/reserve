/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.security.Principal;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * REST Web Service
 *
 * @author Cornel
 */
@Path("cancelReservation")
public class CancelResource {

    @Context
    SecurityContext securityContext;

    /**
     * Creates a new instance of GenericResource
     */
    public CancelResource() {

    }

    @GET
    @Secured
    @Produces(MediaType.TEXT_PLAIN)
    public String getClasses() {

        if (Database.sessions.isEmpty()) {
            return "Nothin to cancel";
        }
        
        Principal principal = securityContext.getUserPrincipal();
        String username = principal.getName();
        NewWorldClassReservation wcr = Database.getUserData(username).wcr;
        if(wcr == null) {
            return "Something went wrong";
        }
        
        if (!wcr.reserveScheduled && !wcr.loginScheduled) {
             return "Nothing to cancel";
        }
        
        if (wcr.reserveScheduled) {
            wcr.reserveTimer.cancel();
            wcr.reserveTimer.purge();
            wcr.reserveScheduled = false;
            wcr.scheduledClass = null;
        }
        
        if (wcr.loginScheduled) {
            wcr.loginTimer.cancel();
            wcr.loginTimer.purge();
            wcr.loginScheduled = false;
        }

        return "Timer canceled !!!";
    }
    
    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putCancelClass(String content) {
    }
}
