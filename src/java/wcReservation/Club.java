/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Cornel
 */
@XmlRootElement
public class Club {
    //ArrayList<Day> days;
    String clubId;
    String clubName;
    boolean hasAccess;
    
    Club(String name, String id, boolean access) {
        clubName = name;
        clubId = id;
        //days = new ArrayList<>();
        hasAccess = access;
    }
    
    Club() {
        clubName = "defaultName";
        clubId = "0";
        //days = new ArrayList<>();
        hasAccess = false;
    }
    
    public String getClubId() {
        return clubId;
    }
    
    public void setClubId(String c) {
        clubId = c;
    }
    
    public String getClubName() {
        return clubName;
    }
    
    public void setClubName(String c) {
        clubName = c;
    }
    
    public boolean getHasAccess() {
        return hasAccess;
    }
    
    public void setHasAccess(boolean a) {
        hasAccess = a;
    }
}
