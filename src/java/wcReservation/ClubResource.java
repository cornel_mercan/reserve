/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * REST Web Service
 *
 * @author Cornel
 */
@Path("clubs")
public class ClubResource {

    @Context
    SecurityContext securityContext;

    /**
     * Creates a new instance of GenericResource
     */
    public ClubResource() {
        //Database.initDatabase();
    }

    @GET
    //@Path("get")
    @Secured
    @Produces(MediaType.APPLICATION_XML)
    public ArrayList<Club> getClubs() {
        
        Principal principal = securityContext.getUserPrincipal();
        String username = principal.getName();
        NewWorldClassReservation wcr = Database.getUserData(username).wcr;
        if(wcr == null) {
            return new ArrayList<>();
        }

        return wcr.clubs;
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putClub(String content) {
    }
}
