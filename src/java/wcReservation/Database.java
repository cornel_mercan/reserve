/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.util.Hashtable;

/**
 *
 * @author Cornel
 */

class UserData {
    NewWorldClassReservation wcr;
    User user;
    String token;
    
    UserData(User u, NewWorldClassReservation w, String t) {
        user = u;
        wcr = w;
        token = t;
    }
}
public class Database {
    
    //static ArrayList<NewWorldClassReservation> sessions;
    static Hashtable<String, User> users = new Hashtable<>();
    static Hashtable<String, UserData> sessions = new Hashtable<>();
    //static boolean initialized = false;
    /*
    public static boolean initDatabase() {
        sessions = new Hashtable<>();
        users = new Hashtable<>();
        return true;
    }
    */
    public static UserData getUserData(String name) {
        if (sessions.containsKey(name))
            return sessions.get(name);
        
        return null;
    }
    
    public static void addUserData(String name, UserData ud) {
        
        sessions.put(name, ud);
    }
    
    public static void removeUserData(String name) {
        sessions.remove(name);
    }
    
    public static User getUser(String token) {
        if (users.containsKey(token))
            return users.get(token);
        
        return null;
    }
    
    public static void addUser(String token, User u) {
        
        users.put(token, u);
    }
    
    public static void removeUser(String token) {
        users.remove(token);
    }
    
    public static String print() {
        String ret = "";
        for (String token: sessions.keySet()) {
            UserData ud = sessions.get(token);
            ret += "Username:" + ud.user.name;
            ret += " Password:" + ud.user.password;
            
            if (!ud.wcr.reserveScheduled) {
                ret += " NoTimer\n";
            }
            else {
                ret += " Scheduled:" + ud.wcr.scheduledClass.title + ", " + ud.wcr.scheduledClass.classDay.name +
                    " on " + ud.wcr.scheduledClass.hours + " at " + ud.wcr.scheduledClass.clubId + "\n";
            }
        }
        
        return ret;
    }
}
