/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.util.ArrayList;
import java.util.Calendar;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Cornel
 */

@XmlRootElement
public class Day {
    String name;
    ArrayList<WCClass> classes;
    
    Day() {
        name = "defaultDay";
        classes = new ArrayList<>();
    }
    
    Day(String n) {
        name = n;
        classes = new ArrayList<>();
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String n) {
        name = n;
    }
    
    public ArrayList<WCClass> getClasses() {
        return classes;
    }
    
    public void setClasses(ArrayList<WCClass> classList) {
        classes = classList;
    }
    
    private int getDay() {
        switch (name) {
            case "Luni":
                return Calendar.MONDAY;
            case "Marti":
                return Calendar.TUESDAY;
            case "Miercuri":
                return Calendar.WEDNESDAY;
            case "Joi":
                return Calendar.THURSDAY;
            case "Vineri":
                return Calendar.FRIDAY;
            case "Sambata":
                return Calendar.SATURDAY;
            case "Duminica":
                return Calendar.SUNDAY;
            default:
                return Calendar.MONDAY;
        }    
    }
    
    public int getReservationDay() {
        int classDay = getDay();
        if (classDay == 1)
            return 7;
        
        return classDay - 1;
    }
}
