/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cornel
 */
public class LoginTask extends TimerTask{
    
    private final String username;
    
    public LoginTask (String user) {
        username = user;
    }

    @Override
    public void run() {
        Logger.getLogger(LoginTask.class.getName()).log(Level.INFO, 
                "Login user " + this.username);       
        
        NewWorldClassReservation wcr = Database.getUserData(this.username).wcr;
        try {
            wcr.login();
        } catch (Exception ex) {
            Logger.getLogger(LoginTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        wcr.loginScheduled = false;
    }
    
}
