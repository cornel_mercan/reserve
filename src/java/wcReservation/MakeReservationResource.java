/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author Cornel
 */
@Path("makeReservation")
public class MakeReservationResource {
    @Context
    SecurityContext securityContext;
    
    
    @GET
    @Secured
    @Path("{classId}")
    @Produces(MediaType.TEXT_PLAIN)
    public String makeReservation(@PathParam("classId") String classId) {
	Principal principal = securityContext.getUserPrincipal();
        String username = principal.getName();
        NewWorldClassReservation wcr = Database.getUserData(username).wcr;
        if(wcr == null) {
            return "Something went wrong";
        }
        
        int id = -1;
        try {
            id = Integer.parseInt(classId);
        } catch (Exception e) {
            Logger.getLogger(ReserveTask.class.getName()).log(Level.SEVERE, "Bad class Id");
        }
        if (id < 0) {
            return "Error: Class not found !";
        }
        WCClass myClass = new WCClass();
        for (Day d : wcr.classes) {
            for (WCClass c : d.classes) {
                if (c.getClassId() == id) {
                    myClass = c;
                    break;
                }
            }
        }
        if (myClass.getClassId() == -1) {
            return "Error: Class not found !";
        }
        
        Calendar reserveCal = Calendar.getInstance();
        int today = reserveCal.get(Calendar.DAY_OF_WEEK);
        
        int reservationDay = myClass.classDay.getReservationDay();
        reserveCal.add(Calendar.DATE, (reservationDay + 7 - today) % 7);
        reserveCal.set(Calendar.HOUR_OF_DAY, myClass.getReservationHour());
        reserveCal.set(Calendar.MINUTE, myClass.getReservationMinute());
        reserveCal.set(Calendar.SECOND, 7);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        
        String reservationString = "Scheduling for " + username + " class:" + myClass.getTitle() + " with " + myClass.getTrainer() + 
                " on " + myClass.getHours() + " at TBD;" + "Trigger at " + sdf.format(reserveCal.getTime());
        Logger.getLogger(ReserveTask.class.getName()).log(Level.INFO, reservationString);
        myClass.setUsername(username);
        
        if (wcr.reserveScheduled && wcr.reserveTimer != null) {
            wcr.reserveTimer.cancel();
            wcr.reserveTimer.purge();
            reservationString += " (old timer removed!)";
            Logger.getLogger(ReserveTask.class.getName()).log(Level.INFO, "Removed old timer");
        }

        wcr.reserveTimer = new Timer();
        wcr.reserveTimer.schedule(new ReserveTask(myClass), reserveCal.getTime());
        wcr.reserveScheduled = true;
        wcr.scheduledClass = myClass;
        
        //schedule login 5 minutes earlier
        Calendar loginCal = reserveCal;
        loginCal.add(Calendar.MINUTE, -5);
        Logger.getLogger(ReserveTask.class.getName()).log(Level.INFO, "Schedule login for " + 
                username + " at " + sdf.format(loginCal.getTime()));
        
        if (wcr.loginScheduled && wcr.loginTimer != null) {
            wcr.loginTimer.cancel();
            wcr.loginTimer.purge();
        }

        wcr.loginTimer = new Timer();
        wcr.loginTimer.schedule(new LoginTask(username), loginCal.getTime());
        wcr.loginScheduled = true;
        
        return reservationString;
    }
    

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void put(String content) {
    }
}
