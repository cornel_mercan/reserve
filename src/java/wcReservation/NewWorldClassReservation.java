/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Cornel
 */
public class NewWorldClassReservation {

    private List<String> cookies;
    private HttpsURLConnection conn;
    private final String USER_AGENT = "Mozilla/5.0";
    ArrayList<Club> clubs;
    ArrayList<Day> classes;
    Timer reserveTimer;
    Timer loginTimer;
    public boolean reserveScheduled;
    public boolean loginScheduled;
    WCClass scheduledClass;
    User thisUser;
    
    public final String basePage = "https://members.worldclass.ro/";
    public final String scheduleUrl = "https://members.worldclass.ro/member-schedule.php"; //?clubId=411&group=-1";
    public final String loginUrl = "https://members.worldclass.ro/_process_login.php";
    public final String reservationUrl = "https://members.worldclass.ro/_book_class.php";//?id=217741&clubid=433";
    
    public NewWorldClassReservation(User u){
        clubs = new ArrayList<>();
        classes = new ArrayList<>();
        reserveScheduled = false;
        loginScheduled = false;
        thisUser = u;
    }

    public String sendPost(String url, String postParams) throws Exception {

	URL obj = new URL(url);
	conn = (HttpsURLConnection) obj.openConnection();

	// Acts like a browser
	conn.setUseCaches(false);
	conn.setRequestMethod("POST");
	conn.setRequestProperty("Host", "members.worldclass.ro");
        conn.setRequestProperty("Origin", "https://members.worldclass.ro");
        conn.setRequestProperty("Referer", "https://members.worldclass.ro/");
	conn.setRequestProperty("User-Agent", USER_AGENT);
	conn.setRequestProperty("Accept",
		"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	for (String cookie : this.cookies) {
		conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
	}
	conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Cache-Control", "max-age=0");
	conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	conn.setRequestProperty("Content-Length", Integer.toString(postParams.length()));

	conn.setDoOutput(true);
	conn.setDoInput(true);

	// Send post request
	DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
	wr.writeBytes(postParams);
	wr.flush();
	wr.close();

	int responseCode = conn.getResponseCode();
	Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Sending 'POST' request to URL : " + url);
	//Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Post parameters : " + postParams);
	Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Response Code : " + responseCode);

	BufferedReader in =
             new BufferedReader(new InputStreamReader(conn.getInputStream()));
	String inputLine;
	StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
	}
	in.close();
        
        return response.toString();
    }
    
    public String GetPageContent(String url) {

	URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            conn = (HttpsURLConnection) obj.openConnection();
        } catch (IOException ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            // default is GET
            conn.setRequestMethod("GET");
        } catch (ProtocolException ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }

	conn.setUseCaches(false);

	// act like a browser
        conn.setRequestProperty("Host", "members.worldclass.ro");
        conn.setRequestProperty("Referer", "https://members.worldclass.ro/");
	conn.setRequestProperty("User-Agent", USER_AGENT);
	conn.setRequestProperty("Accept",
		"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        if (cookies != null) {
            for (String cookie : this.cookies) {
                    conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }
        }
	conn.setRequestProperty("Connection", "keep-alive");
        conn.setRequestProperty("Cache-Control", "max-age=0");
	conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        
	int responseCode = 0;
        try {
            responseCode = conn.getResponseCode();
        } catch (IOException ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
	Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "\nSending 'GET' request to URL : " + url);
	Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Response Code : " + responseCode);

	BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } catch (IOException ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
	String inputLine;
	StringBuilder response = new StringBuilder();

        try {
            while (null != (inputLine = in.readLine())) {
                response.append(inputLine);
            }
        } catch (IOException ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }

	// Get the response cookies
	setCookies(conn.getHeaderFields().get("Set-Cookie"));

	return response.toString();
    }
    
    public List<String> getCookies() {
	return cookies;
    }

    public void setCookies(List<String> cookies) {
        
        if (cookies == null || cookies.isEmpty())
            return;
        this.cookies = new ArrayList<>();
        cookies.forEach((c) -> {
            this.cookies.add(c);
        });
        
	this.cookies = cookies;
        //this.cookies.add("_ga=GA1.2.319832235.1491509620; _gat=1");
    }
    
    public ArrayList<Day> getAllClasses(String clubId){
        ArrayList<Day> days = new ArrayList<>();
        
        if (!hasClubId(clubId))
            return days;
        
        String thisClubHtml = "";
        try {
            thisClubHtml = sendPost(this.scheduleUrl, "clubid=" + clubId + "&group=-1");
        } catch (Exception ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
	//Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Extracting classes from this club's data...");
        
	Document doc = Jsoup.parse(thisClubHtml);
        
        Element eSchedule = doc.getElementById("schedule-carousel");
        Elements eDays = eSchedule.getElementsByClass("daily-schedule");
        
        int id = 0;
        
        for (Element eDay : eDays) {
            Elements eWeekday = eDay.getElementsByClass("weekday");
            String dayName = eWeekday.first().text();
            Day d = new Day(dayName);
            Elements eClasses = eDay.getElementsByClass("schedule-class");
            for (Element eClass : eClasses) {
                String hour = eClass.getElementsByClass("class-hours").first().text();
                String title = eClass.getElementsByClass("class-title").first().text();
                String trainer = eClass.getElementsByClass("trainers").first().text();
                //String response = eClass.getElementsByClass("alert alert-x-success").first().text();
                //String response = eClass.getElementsByClass("alert alert-x-danger").first().text();
                
                String target = "";
                Elements eTarget = eClass.getElementsByClass("btn btn-book-class");
                if (!eTarget.isEmpty()) {
                    target = eTarget.first().attr("data-target");
                }
                d.classes.add(new WCClass(title, hour, trainer, target, id++, d, clubId));
            }
            
            days.add(d);
        }
        
        this.classes = days;
        
	return days;
    }
    
    //clubs will not contain classes at this point
    public ArrayList<Club> getClubs(){
        
        String html = GetPageContent(scheduleUrl);

	//Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Extracting form's data...");
        ArrayList<Club> clubs = new ArrayList<>();

	Document doc = Jsoup.parse(html);
        
        Element eClubSelect = doc.getElementById("box-select-club");
        Element eClubs = eClubSelect.getElementById("clubid");
        Elements options = eClubs.getElementsByTag("option");
        options.forEach((o) -> {
            String id = o.attr("value");
            String name = o.text();
            boolean access = false;
            String classAccess = o.attr("class");
            if (classAccess.equals("has-access")) {
                access = true;
            }
            Club c = new Club(name, id, access);
            clubs.add(c);
        });
        
        this.clubs = clubs;
        
	return clubs;
    }
    
    public String getResponse(String html){
	Document doc = Jsoup.parse(html);
        
        Elements alerts = doc.getElementsByClass("alert alert-x-success");
        if (!alerts.isEmpty()) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Alert: " + alerts.first().text());
            return alerts.first().text();
        }
        
        alerts = doc.getElementsByClass("alert alert-x-danger");
        if (!alerts.isEmpty()) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Alert: " + alerts.first().text());
            return alerts.first().text();
        }
        
	return "No alert in reponse";
    }
    
    private boolean hasClubId(String clubId) {
        if (clubs.stream().anyMatch((c) -> (c.getClubId().equals(clubId)))) {
            return true;
        }
        return false;
    }
    
    public String reserveClass(String classId, String clubId) {

        Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, "Reserving " + classId + "&" + clubId);
        String page = "";
        String reservation = reservationUrl + "?id=" + classId + "&clubid=" + clubId;

        page = this.GetPageContent(reservation);
        
        return getResponse(page);
    }
    
    public void login() throws Exception {

        Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.INFO, 
                "Login user " + thisUser.name);
        CookieHandler.setDefault(new CookieManager());
        
	// 1. Send a "GET" request, so that you can extract the form's data.
        String page = "";
        String cornelUser = "email=camercan@gmail.com&pincode=0709";
        String loginStr = "email="+thisUser.name+"&pincode="+thisUser.password;
        try {
            page = GetPageContent(basePage);
        } catch (Exception ex) {
            Logger.getLogger(NewWorldClassReservation.class.getName()).log(Level.SEVERE, null, ex);
        }

        sendPost(loginUrl, loginStr);
        getClubs();
         
        
        //if (!200OK)throw Exception
    }
}
