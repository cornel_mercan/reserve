/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

import java.util.ArrayList;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cornel
 */
public class ReserveTask extends TimerTask{
    
    private WCClass wcClass;
    
    public ReserveTask (WCClass c) {
        wcClass = c;
    }

    @Override
    public void run() {
        Logger.getLogger(ReserveTask.class.getName()).log(Level.INFO, 
                "Reserving for " + wcClass.username + ": " + wcClass.getTitle() + " with " + 
                wcClass.getTrainer() + " on " + wcClass.getHours() + " at TBD");
        
        
        NewWorldClassReservation wcr = Database.getUserData(wcClass.username).wcr;
        //At this point user should be logged in
        
        //search for selected class in the actual list
        ArrayList<Day> currentClasses = wcr.getAllClasses(wcClass.clubId);
        currentClasses.forEach((d) -> {
            d.classes.stream().filter((c) -> (c.classDay.name.equals(wcClass.classDay.name) &&
                    c.title.equals(wcClass.title) &&
                    c.hours.equals(wcClass.hours))).forEachOrdered((c) -> {
                        wcClass = c;
                        Logger.getLogger(ReserveTask.class.getName()).log(Level.INFO, 
                            "Found class with target=" + wcClass.getTarget()); 
            });

        });
        
        if (wcClass.getTarget() == null) {
            Logger.getLogger(ReserveTask.class.getName()).log(Level.SEVERE, 
                "Class has no target; Could not make reserve!");
            wcr.reserveScheduled = false;
            return;
        }
        String classId = wcClass.getClassTargetId();
        String result = wcr.reserveClass(classId, wcClass.clubId);
        Logger.getLogger(ReserveTask.class.getName()).log(Level.INFO, result);
        
        wcr.reserveScheduled= false;
    }
    
}
