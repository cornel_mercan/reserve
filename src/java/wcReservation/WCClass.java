/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wcReservation;

/**
 *
 * @author Cornel
 */

import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WCClass {
    String title;
    String hours;
    String trainer;
    String target;
    int classId;
    Day classDay;
    String clubId;
    String username;
     
    
    WCClass (String t, String h, String tr, String ta, int id, Day day, String _clubId) {
        title = t;
        hours = h;
        trainer = tr;
        target = ta;
        classId = id;
        classDay = day;
        clubId = _clubId;
    }
    
    WCClass () {
        title = "dummyTitle";
        hours = "00:00";
        trainer = "dummyTrainer";
        target = "";
        classId = -1;
        classDay = new Day();
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String s) {
        title = s;
    }
    
    public String getHours() {
        return hours;
    }
    
    public void setHours(String s) {
        hours = s;
    }
    
    public String getTrainer() {
        return trainer;
    }
    
    public void setTrainer(String s) {
        trainer = s;
    }
    
    public String getTarget() {
        return target;
    }
    
    public void setTarget(String s) {
        target = s;
    }
    
    public int getClassId() {
        return classId;
    }
    
    public void setClassId(int id) {
        classId = id;
    }
    
    public int getReservationHour() {
        StringTokenizer st = new StringTokenizer(hours," :-");
        String strHour = st.nextToken();
        return Integer.parseInt(strHour) - 2;
    }
    
    public int getReservationMinute() {
        StringTokenizer st = new StringTokenizer(hours," :-");
        st.nextToken();
        String strMinute = st.nextToken();
        return Integer.parseInt(strMinute);
    }
    
    public String getClassTargetId() {
        StringTokenizer st = new StringTokenizer(target,"-");
        String strId = "";
        try {
            st.nextToken();
            strId = st.nextToken();
        }
        catch (Exception ex) {
            Logger.getLogger(WCClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Logger.getLogger(WCClass.class.getName()).log(Level.INFO, "Target is " +  strId);
        return strId;
    }
    
    void setUsername(String s) {
        username = s;
    }
}
